This module implements a refund command that differs from the default refund
command in that it does not merely delete refunded lines from the order.
Instead refunded lines are given a negative price.

WARNING:
--------

If you use this module, the reports Ubercart 6.x provides will contain
incorrect data. You should only use this module if your site contains some
other method for reporting or if you don't use reporting at all.

REQUIREMENTS:
------------

This module requires the most recent 6.1x-dev branch of the UberPOS module.
