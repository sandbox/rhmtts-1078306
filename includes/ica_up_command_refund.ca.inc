<?php

/**
 * @file
 * Contains conditional action functions for removing products from 
 * orders
 */

/**
 * Implementation of hook_action().
 */
function ica_up_command_refund_ca_action() {
  // no need to have one for decrementing stock, as we can simply mutate the 
  // stock when the command is executed.
  $actions['ica_up_command_refund_action_increment_stock'] = array(
    '#title' => t('Increment stock of a product from a specific store (refund-proof).'),
    '#callback' => 'ica_up_command_refund_action_increment_stock',
    '#arguments' => array(
      'order' => array('#entity' => 'uc_order', '#title' => t('Order')),
      'product' => array('#entity' => 'uc_order_product', '#title' => t('Product')),
    ),
    '#category' => t('Stock'),
  );
  return $actions;
}

function ica_up_command_refund_action_increment_stock($order, $product, $settings) {
  if(module_exists('up_multi')) {
    $store = $order->store_id;
    if (($stock = up_multi_stock($product->model, $store)) !== FALSE) {
      $qty = $product->qty;
      if($product->price < 0) {
        $qty *= -1;
      };
      up_multi_stock_adjust($product->model, $qty, $store);
      uc_order_comment_save($order->order_id, $user->uid, t('The stock level for %model_name at location %store has been increased to !qty.', array('%model_name' => $product->title, '%store' => $store, '!qty' => ($stock - $qty))));
    }
  }
  // no multiple locations
  else {
    if (($stock = uc_stock_level($product->model)) !== FALSE) {
      $qty = $product->qty;
      if($product->price < 0) {
        $qty *= -1;
      };
      uc_stock_adjust($product->model, $qty);
      uc_order_comment_save($order->order_id, 0, t('The stock level for %model_name has been increased to !qty.', array('%model_name' => $product->model, '!qty' => ($stock - $qty))));
    }

  }
}
